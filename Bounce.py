from Bounce_item import *

from Bounce_State import *
from pygame.locals import *
import Leap, sys, pygame, math, timer

SCREEN_SIZE = (1000, 600)
BG_COLOR = (106, 189, 255)

class GameScreen():

  def __init__(self):

    # LEAP motion initialization
    self.controller = Leap.Controller()
    self.controller.enable_gesture(Leap.Gesture.TYPE_SWIPE)    
  
    # pygame initialization
    pygame.init()
    pygame.display.set_caption("Bounce")
    self.screen = pygame.display.set_mode(SCREEN_SIZE)


    self.state = IntroState(self.screen, self)

    # self.state = HowToState(self.screen, self)

    self.gameState = GameState(self.screen, self)
    # self.state = self.gameState

    
  def start(self):
    while  True:
      frame = self.controller.frame()
      self.processInput(frame)
      self.update(frame)
      self.render()

  def processInput(self, frame):
    self.hands = frame.hands
    for event in pygame.event.get():
        self.event_handler(event)
    self.state.processInput(frame)

  def update(self, frame):

    if(len(self.hands) == 0 and self.state.checkHand):
      self.previousState = self.state
      if(self.previousState.__class__ == GameState):
        if(self.state.timer.isAlive()):
          self.state.timer.stop()
      self.state = PauseState(self.screen, self, self.previousState)

    elif(self.state.__class__ == PauseState and len(self.hands) > 0):
      self.state = self.previousState
      if(self.state.__class__ == GameState):
        self.state.restartTimer()
    self.state.update(frame)

  def render(self):
    self.state.render()
    pygame.display.flip()

  def goToGameState(self):
    self.state = self.gameState
    self.state.reset()

  def goToMenuState(self):
    self.state = MenuState(self.screen, self)
    self.state.playBGM()

  def goToHowToState(self):
    self.state = HowToState(self.screen, self)

  def goToCreditState(self):
    self.state = CreditState(self.screen, self)

  def quit(self):
    if(self.state.__class__ == GameState):
      self.state.timer.stop()
    pygame.quit()
    sys.exit(0)

  def event_handler(self, event):
    if event.type == pygame.QUIT :
      self.quit()
    if event.type == KEYDOWN and event.key == K_s:
      self.quit()
    if event.type == KEYDOWN and event.key == K_ESCAPE:
      if self.screen.get_flags() & FULLSCREEN:
        pygame.display.set_mode(SCREEN_SIZE)
      else:
        pygame.display.set_mode(SCREEN_SIZE, FULLSCREEN)

# <Event(2-KeyDown {'scancode': 0, 'mod': 0, 'key': 310, 'unicode': ''})>
# <Event(2-KeyDown {'scancode': 12, 'mod': 1024, 'key': 113, 'unicode': 'q'})>

