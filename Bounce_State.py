import Leap, sys, pygame, math, time
from Bounce_item import *
from Bounce_model import *
from player import *
from timer import *

SCREEN_SIZE = (1000, 600)

class State():

  checkHand = True

  def __init__(self, screen):
    self.screen = screen
    self.x = 5

  def processInput(self, frame):
    return

  def update(self):
    return

  def render(self):
    return


  def isGrab(self, hand):
    if(hand.grab_strength == 1):
      return True
    else:
      return False

class IntroState(State):

  def __init__(self, screen, bounce):
    self.screen = screen
    self.bounce = bounce
    self.index = 1
    self.checkHand = False
    self.background = pygame.image.load("resources/image/firstpage/black.png").convert()
    self.csBG = pygame.image.load("resources/image/firstpage/cs.png").convert()
    self.alpha = 255    
    self.clock = pygame.time.Clock()
    self.looper = 0
    self.loadBG()

  def processInput(self, frame):
    return

  def update(self, frame):


    if(self.index >= 8):
      self.index = 1
      if(self.alpha <= 0):
        self.looper += 1
      if(self.looper == 3):
        self.goToMenuState()
    else:
      self.index  += 1

    if(self.alpha >= 0):
      self.clock.tick(30)
      self.alpha -= 3
      # self.alpha -= 20
    else:
      self.clock.tick(3)
      self.csBG = self.panels[self.index-1]

    self.background.set_alpha(self.alpha)

  def render(self):
    self.screen.blit(self.csBG, [0, 0])
    self.screen.blit(self.background, [0, 0])

  def loadBG(self):
    self.panels = []
    for x in range(1,9):
      panel = pygame.image.load("resources/image/firstpage/" + str(x) + ".png").convert()
      self.panels.append(panel)

  def goToMenuState(self):
    self.bounce.goToMenuState()

class MenuState(State):
  
  def __init__(self, screen, bounce):
    self.screen = screen
    self.bounce = bounce
    self.background = pygame.image.load("resources/image/menu/menu.png")
    self.bgm = pygame.mixer.Sound("resources/sound/main.wav")
    self.addButtons()
    self.cursor = Cursor()
    self.balls = []
    self.generateMouse()

  def addButtons(self):
    self.buttons = []
    self.play = PlayButton(441, 137,self.bounce)
    self.howTo = HowToButton(441, 247, self.bounce)
    self.credit = CreditButton(441, 357, self.bounce)
    self.exit = ExitButton(441, 467, self.bounce)
    self.buttons.append(self.play)
    self.buttons.append(self.howTo)
    self.buttons.append(self.credit)
    self.buttons.append(self.exit)

  def generateMouse(self):
    for i in range(0,4):
      x = random.randint(60, SCREEN_SIZE[0] - 60)
      y = random.randint(60, SCREEN_SIZE[1])
      ball = Ball(self.background, i+1)
      ball.loadImage(i+1)
      ball.rect.midbottom = (x, y)
      ball.randomTurn()
      self.balls.append(ball)


  def playBGM(self):
    self.bgm.play(-1)

  def processInput(self, frame):
    self.hand = frame.hands[0]

  def update(self, frame):
    
    # self.clock.tick(40)
    interactionBox = frame.interaction_box
    position = interactionBox.normalize_point(self.hand.palm_position)
    x = position.x * SCREEN_SIZE[0]      
    y = SCREEN_SIZE[1] - (position.y * SCREEN_SIZE[1])
    
    self.cursor.setPos(x, y)

    for button in self.buttons:
      if self.dragButton(button, self.cursor, self.hand):
        if button.isAtTheEnd():
          self.bgm.stop()
          button.goToNextState()
        break

    for ball in self.balls:
      ball.move()
      ball.jump()



  def render(self):
    self.screen.blit(self.background, [0, 0])
    # self.screen.blit(self.play.image, self.play.rect)
    for button in self.buttons:
      self.screen.blit(button.image, button.rect)
    for ball in self.balls:
      self.screen.blit(ball.image, ball.rect)
    self.screen.blit(self.cursor.image, self.cursor.rect)


  def dragButton(self, button, cursor, hand):
    # print(pygame.sprite.collide_rect(button, hand))
    if(pygame.sprite.collide_rect(button, cursor) and self.isGrab(hand)):
      button.setPos(self.cursor.rect.centerx)
      self.cursor.image = self.cursor.image2
      return True
    else:
      self.cursor.image = self.cursor.image1
      return False

class HowToState(State):

  def __init__(self, screen, bounce):
    self.screen = screen
    self.bounce = bounce
    self.rope = Rope(947, 0)
    self.sneeze = pygame.mixer.Sound("resources/sound/sneeze.wav")
    self.loadBG()
    self.index = 0
    self.background = self.bg[self.index]
    self.border = pygame.image.load("resources/image/howto/border.png").convert_alpha()
    self.itemZone = pygame.image.load("resources/image/howto/itemzone.png").convert_alpha()
    self.bgm = pygame.mixer.Sound("resources/sound/main.wav")
    self.leftHand = ""
    self.rightHand = ""
    self.specialItemFactory = SpecialItemFactory()
    self.specialItems = []
    self.cursor = Cursor()
    self.back = BackButton(529, 523, self.bounce)
    self.paddle = Paddle(self.border)
    self.paddle.rect.centerx = 300
    self.paddle.rect.centery = 401
    self.block = 0
    self.bgm.play(-1)

  def processInput(self, frame):
    self.hands = frame.hands

    for hand in self.hands:
      if(hand.is_left):
        self.leftHand = hand
      elif(hand.is_right):
        self.rightHand = hand

  def update(self, frame):

    interactionBox = frame.interaction_box
    if(type(self.rightHand) == Leap.Hand):
      
      gesture = frame.gestures()[0]
      self.checkGesture(gesture)

      position = interactionBox.normalize_point(self.rightHand.palm_position)
      x = position.x * SCREEN_SIZE[0]      
      y = SCREEN_SIZE[1] - (position.y * SCREEN_SIZE[1])
      self.cursor.setPos(x, y)
      self.dragButton(self.back, self.cursor, self.rightHand)
      self.pullRope(self.rope, self.cursor, self.rightHand)


      if self.back.isAtTheEnd():
        self.bgm.stop()
        self.back.goToNextState()

      if self.rope.isAtTheEnd():
        self.rope.rect.bottom = self.rope.start
        if(self.index < 2):
          self.index += 1
          self.background = self.bg[self.index]

    if(type(self.leftHand) == Leap.Hand):
      position = interactionBox.normalize_point(self.leftHand.palm_position)
      y =  555 - (position.y * SCREEN_SIZE[1])
      bound = 133 + self.border.get_width() - self.paddle.image.get_width()
      x = 5
      parsedPosition = position.x * 2.5 * bound
      if(parsedPosition > bound ):
        x = bound
      elif(parsedPosition <= 140):
        x = 140
      else :
        x = position.x * 2.5 * bound

      if(y  > 430 - self.paddle.rect.height):
        y = 430 - self.paddle.rect.height

      self.paddle.setPos(x, y)
      gesture = frame.gestures()[0]
    now = int(time.time())
    if( not self.specialItems and now > self.block):
      self.specialItems.append(self.specialItemFactory.createItem(10, 1))
      self.specialItems[0].rect.x = 585
      self.specialItems[0].rect.y = 175

  def render(self):
    self.screen.blit(self.background, [0, 0])
    if(self.index == 0):
      self.screen.blit(self.border, [135, 127])
      self.screen.blit(self.itemZone, [537, 115])
      self.screen.blit(self.paddle.image, self.paddle.rect)
      for item in self.specialItems:
        self.screen.blit(item.image, item.rect)

    self.screen.blit(self.back.image, self.back.rect)
    self.screen.blit(self.rope.image, self.rope.rect)
    self.screen.blit(self.cursor.image, self.cursor.rect)


  def loadBG(self):
    self.bg = []
    for i in range(1, 4):
      background = pygame.image.load("resources/image/howto/howtoBG"+str(i)+".png").convert()
      self.bg.append(background)

  def dragButton(self, button, cursor, hand):
    if(pygame.sprite.collide_rect(button, cursor) and self.isGrab(hand)):
      button.setPos(self.cursor.rect.centerx)
      self.cursor.image = self.cursor.image2
    else:
      self.cursor.image = self.cursor.image1

  def pullRope(self, button, cursor, hand):
    if(pygame.sprite.collide_rect(button, cursor) and self.isGrab(hand)):
      button.setPos(self.cursor.rect.centery)
      self.cursor.image = self.cursor.image2
    else:
      self.cursor.image = self.cursor.image1


  def checkGesture(self, gesture):

    if(gesture.is_valid):
      self.eliminateObstacle(gesture)
      now = int(time.time())
      self.block = now + 5
    if(self.isGrab(self.rightHand) and self.specialItems):
      self.specialItems.pop(0)
      now = int(time.time())
      self.block = now + 5


  def eliminateObstacle(self, gesture):
    if(self.isSwipe(gesture) and len(self.specialItems) > 0):
      del self.specialItems[:]
    effectChannel = pygame.mixer.Channel(5)
    if(not effectChannel.get_busy()):
      effectChannel.play(self.sneeze)

  def isSwipe(self, gesture):
    gestureType = gesture.type
    if(gestureType is Leap.Gesture.TYPE_SWIPE ):
      # swipe = Leap.SwipeGesture(gesture)
      # print(swipe.direction)
      return True
    return False

class CreditState(State):
  def __init__(self, screen, bounce):
    self.screen = screen
    self.bounce = bounce
    self.background = pygame.image.load("resources/image/credit/credit.png")
    self.bgm = pygame.mixer.Sound("resources/sound/main.wav")
    self.back = BackButton(509, 513, self.bounce)
    self.cursor = Cursor()
    self.bgm.play(-1)

  def processInput(self, frame):
    self.hand = frame.hands[0]

  def update(self, frame):
    interactionBox = frame.interaction_box
    position = interactionBox.normalize_point(self.hand.palm_position)
    x = position.x * SCREEN_SIZE[0]      
    y = SCREEN_SIZE[1] - (position.y * SCREEN_SIZE[1])
    
    self.cursor.setPos(x, y)
    self.dragButton(self.back, self.cursor, self.hand)

    if self.back.isAtTheEnd():
      self.bgm.stop()
      self.back.goToNextState()

  def render(self):
    self.screen.blit(self.background, [0, 0])
    self.screen.blit(self.back.image, self.back.rect)
    self.screen.blit(self.cursor.image, self.cursor.rect)

  def dragButton(self, button, cursor, hand):
    # print(pygame.sprite.collide_rect(button, hand))
    if(pygame.sprite.collide_rect(button, cursor) and self.isGrab(hand)):
      button.setPos(self.cursor.rect.centerx)

class PauseState(State):

  def __init__(self, screen, bounce, previousState):
    self.screen = screen
    self.bounce = bounce
    self.checkHand = False
    self.previousState = previousState
    self.index = 0
    self.clock = pygame.time.Clock()
    self.loadBG()


  def processInput(self, frame):
    self.hands = frame.hands


  def update(self, frame):
    self.index += 1
    if(self.index >  8):
      self.index = 1


  def render(self):
    self.previousState.render()
    self.clock.tick(5)
    self.screen.blit(self.panels[self.index-1], [0, 0])

  def loadBG(self):
    self.panels = []
    for x in range(1,9):
      panel = pygame.image.load("resources/image/pause/pause" + str(x) + ".png")
      self.panels.append(panel)

class GameOverState(State):

  def __init__(self, screen, bounce, gameState, win):
    self.screen = screen
    self.bounce = bounce
    self.gameState = gameState
    self.score = gameState.score
    self.checkHand = False
    self.font = pygame.font.Font("CHERI.TTF", 35)
    self.index = 0
    self.clock = pygame.time.Clock()
    self.loadBG()
    self.win = win
    self.bonus = 0
    self.totalScore = 0
    self.calculateScore()
    self.coinSound = pygame.mixer.Sound("resources/sound/coin.wav")
    self.scoreLabel = self.font.render(str(self.score), True, (55,176,127))
    self.bonusTimeLabel = self.font.render(str(self.bonus), True, (255,161,0))
    self.totalScoreLabel = self.font.render(str(self.totalScore), True, (255,83,95))
    self.previousScore1 = 0
    self.previousScore2 = 0
    self.previousScore3 = 0

  def updateScore(self):
    if(self.score > self.previousScore1):
      different = self.score - self.previousScore1
      interval = different/2
      self.previousScore1 += interval + 1
    else:
      self.previousScore1 = self.score
    self.scoreLabel = self.font.render(str(self.previousScore1), True, (55,176,127))

  
  def updateBonus(self):
    if(self.bonus > self.previousScore2):
      different = self.bonus - self.previousScore2
      interval = different/2
      self.previousScore2 += interval + 1
    else:
      self.previousScore2 = self.bonus
    self.bonusTimeLabel = self.font.render(str(self.previousScore2), True, (255,161,0))
      
  def updateTotalScore(self):
    if(self.totalScore > self.previousScore3):
      different = self.totalScore - self.previousScore3
      interval = different/2
      self.previousScore3 += interval + 1
      self.coinSound.play(1)
    else:
      self.previousScore3 = self.totalScore
    self.totalScoreLabel = self.font.render(str(self.previousScore3), True, (255,83,95))

  def calculateScore(self):
    if self.win:
      self.bonus = self.gameState.time * (self.gameState.stage * 60)
    self.totalScore = self.bonus + self.score

  def loadBG(self):
    self.panels = []
    for x in xrange(1,5):
      panel = pygame.image.load("resources/image/gameover/gameover" + str(x) + ".png")
      self.panels.append(panel)

  def processInput(self, frame):
    self.gesture = frame.gestures()[0]

  def update(self, frame):

    self.updateScore()
    self.updateBonus()
    self.updateTotalScore()
    self.index += 1
    if(self.index > 4 ):
      self.index = 1

    gestureType = self.gesture.type
    
    if(gestureType is Leap.Gesture.TYPE_SWIPE ):
      self.bounce.goToGameState()

  def render(self):
    self.gameState.render()
    self.clock.tick(10)
    self.screen.blit(self.panels[self.index-1], [0, 0])
    self.screen.blit(self.scoreLabel, (510,228))
    self.screen.blit(self.bonusTimeLabel, (510,265))
    self.screen.blit(self.totalScoreLabel, (510,301))
    
class GameState(State):

  def __init__(self, screen, bounce):
    self.screen = screen
    self.bounce = bounce

    # xxxxxxxxxxxxxxxxxxxxxx
    # self.reset()
    # xxxxxxxxxxxxxxxxxxxxxxx

  def reset(self):
    self.leftHand = 0
    self.rightHand = 0
    self.hands = []
    self.bricks = self.generateBricks()
    self.score = 0
    self.items = []
    self.time = 60
    self.stage = 1
    self.specialItemFactory = SpecialItemFactory()
    self.specialItems = []
    self.player = Player()
    self.clock = pygame.time.Clock()
    self.font = pygame.font.Font("CHERI.TTF", 50)
    self.fontWater = pygame.font.Font("Watermelon.ttf", 45)
    self.characterList = []
    self.sneeze = pygame.mixer.Sound("resources/sound/sneeze.wav")
    self.hitSound = pygame.mixer.Sound("resources/sound/crash.wav")
    self.background = pygame.image.load("resources/image/stage1/background.png").convert()
    self.backgroundStage = pygame.image.load("resources/image/stage1/background_game.png").convert()
    self.itemZone = pygame.image.load("resources/image/stage1/itemzone.png").convert_alpha()
    self.gauge = pygame.image.load("resources/image/gauge.png").convert_alpha()
    self.dialog = pygame.image.load("resources/image/swipetostart.png").convert_alpha()
    self.balls = []
    self.balls.append(Ball(self.backgroundStage, self.stage))
    self.bullet = []
    self.paddle = Paddle(self.backgroundStage)
    self.putBallToPaddle()
    self.ready = False
    self.addBGMs()
    self.currentMusic = self.bgm[0]
    self.currentMusic.play(-1)
    self.timer = Timer(self)
    self.score_Label = self.font.render(str(self.score), True, (0,255,0))
    self.lifeLabel = self.font.render(str(self.player.life), True, (255,0,0))
    self.timeLabel = self.font.render(str(self.time), True, (255,255,0))
    self.stageLabel = self.fontWater.render(str(self.stage), True, (255, 0, 114))

    self.resetSpecialItemList()
    self.previousScore = 0

  def processInput(self, frame):
    self.hands = frame.hands
    
    for hand in self.hands:
      if(hand.is_left):
        self.leftHand = hand
      # elif(len(self.hands) > 1):
      elif(hand.is_right):
        self.rightHand = hand

  def update(self, frame):

    # print(self.clock.get_fps())
    self.clock.tick(25)
    interactionBox = frame.interaction_box
    self.timeLabel = self.font.render(str(self.time), True, (255,255,0))
    
    if len(self.hands) > 0:
      if(type(self.rightHand) == Leap.Hand):
        
        gesture = frame.gestures()[0]
        self.checkGesture(gesture)

      if(type(self.leftHand) == Leap.Hand):
        position = interactionBox.normalize_point(self.leftHand.palm_position)
        y =  555 - (position.y * SCREEN_SIZE[1])
        bound = self.backgroundStage.get_width() - self.paddle.image.get_width()
        x = 5
        if(position.x * 2.5 * bound > bound ):
          x = bound
        else :
          x = position.x * 2.5 * bound

        self.paddle.setPos(x, y)
        gesture = frame.gestures()[0]

        if self.gameOver():
          return
        
        self.checkGoToNextStage()
        self.wait(gesture)
        self.score_Label = self.font.render(str(self.score), True, (0,255,0))
        self.lifeLabel = self.font.render(str(self.player.life), True, (255,0,0))

    
        if(self.ready):
          for ball in self.balls:      
            ball.move()
            if(self.isOutOfScreen(ball)):
              self.balls.remove(ball)
              if(len(self.balls) == 0):
                self.player.life -= 1
                self.resetBall()
                self.ready = False
            self.collideWithPaddle(ball)
          for item in self.items:
            item.move()
            self.collideWithPaddle(item)
            if(self.isOutOfScreen(item)):
              self.items.remove(item)

          for bullet in self.bullet:
            bullet.move()    
            if(self.isOutOfScreen(bullet)):
              del self.bullet[:]

          self.shoot()
          self.checkSpecialItem()
          self.collideWithBricks(self.bricks)
          self.generateSpecialItem()
          self.updateScore()
    
        else:
          self.timer.stop()
          paddleWidth = self.paddle.image.get_width()
          self.balls[0].rect.midbottom = (x+paddleWidth/2, self.paddle.rect.y)

  def render(self):
    
    self.screen.blit(self.background, [0, 0])
    self.screen.blit(self.backgroundStage, [6,6])
    self.screen.blit(self.itemZone, [683, 242])
    self.screen.blit(self.gauge, [700,6])
    self.screen.blit(self.paddle.image, self.paddle.rect)

    for ball in self.balls:
      self.screen.blit(ball.image, ball.rect)

    for item in self.specialItems:
      self.screen.blit(item.image, item.rect)

    for item in self.items:
      self.screen.blit(item.image, item.rect)

    for bullet in self.bullet:
      self.screen.blit(bullet.image, bullet.rect)

    for brick in self.bricks:
      self.screen.blit(brick.image, brick.rect)

    if(not self.ready):
      self.screen.blit(self.dialog, [155, 242])
    
    self.screen.blit(self.score_Label, (783, 23))
    self.screen.blit(self.timeLabel, (783, 100))
    self.screen.blit(self.lifeLabel, (783, 177))
    self.screen.blit(self.stageLabel, (830, 263))


  def updateScore(self):
    if(self.score > self.previousScore):
      different = self.score - self.previousScore
      interval = different/2
      self.score_Label = self.font.render(str(self.previousScore), True, (0,255,0))
      self.previousScore += interval + 1
    else:
      self.previousScore = self.score
    
  def collideWithPaddle(self, obj):
    if(pygame.sprite.collide_rect(self.paddle, obj) and (type(obj) == Ball) ):
      obj.speed[1] = math.fabs(obj.speed[1]) * -1
      self.hitSound.play()

    elif(pygame.sprite.collide_rect(self.paddle, obj) and (issubclass(obj.__class__, Item)) ):
      crashedItemIndex = self.paddle.rect.collidelist(self.items)
      self.items[crashedItemIndex].sound.play()
      self.items[crashedItemIndex].onHit(self)
      self.items.pop(crashedItemIndex)

  def restartTimer(self):
    if(self.timer.isAlive()):
      self.timer.stop()
    self.timer = Timer(self)
    self.timer.start()

  def collideWithBricks(self, bricks):  
    score = 50 * self.stage
    for ball in self.balls:
      index = ball.rect.collidelist(bricks)
      if index >= 0:
        if not ball.isBig:
          ball.speed[1] = -ball.speed[1]
        x = bricks[index].rect.centerx
        y = bricks[index].rect.centery
        item = bricks[index].randomItem(x, y, self.stage)
        if item != False:
          self.items.append(item)
        bricks.pop(index)
        self.score += score
        self.hitSound.play()

    for bullet in self.bullet:
      index = bullet.rect.collidelist(bricks)
      if index >= 0:
        bricks.pop(index)
        self.score += score
        self.hitSound.play()
        del self.bullet[:]

  def wait(self, gesture):
    if(self.isSwipe(gesture) and self.ready == False):
      self.restartTimer()
      self.ready = True

  def resetBall(self):
    self.balls = []
    self.balls.append(Ball(self.backgroundStage, self.stage))

  def checkSpecialItem(self):
    for item in self.specialItems:
          item.update(self.time)
          if item.timeUp():
            item.afterTimeUp(self)
            self.specialItems.remove(item)   

  def checkGesture(self, gesture):

    if(gesture.is_valid):
      self.eliminateObstacle(gesture)

    if(self.isGrab(self.rightHand) and self.specialItems):
      self.specialItems[0].effect(self)
      self.specialItems.pop(0)

  def gameOver(self):
    winLastStage = (not self.bricks) and (self.stage == 20)
    if self.player.life < 1 or winLastStage:
      self.currentMusic.stop()
      self.timer.stop()
      gameOverState = GameOverState(self.screen, self.bounce, self, not self.bricks)
      self.bounce.state = gameOverState
      return True
    return False

  def popBalls(self):
    size = len(self.balls)
    for ball in range(0, size-1):
      self.balls.pop()

  def calculateBonus(self):
    bonus = self.time * (self.stage * 60)
    return bonus

  def checkGoToNextStage(self):
    if not self.bricks:

      self.popBalls()
      self.putBallToPaddle()
      if self.stage % 5 == 0:
        parsedStage = str(int(math.ceil((self.stage + 1) / 5.0)))
        self.changeBG(parsedStage)
        self.changePaddle(parsedStage)
        self.changeBall(parsedStage)
        self.changeBricks(parsedStage)
        self.currentMusic.stop()
        self.currentMusic = self.bgm[int(parsedStage)-1]
        self.currentMusic.play(-1)
      else:
        parsedStage = str(int(math.ceil(self.stage/5.0)))
        self.bricks = self.generateBricks(parsedStage)
      self.stage += 1
      self.score += self.calculateBonus()  
      self.time += 30
      self.stageLabel = self.fontWater.render(str(self.stage), True, (255, 0, 114))
      if(self.time == 30):
        self.restartTimer()

  def changeBG(self, stage):
    self.background = pygame.image.load("resources/image/stage"+stage+"/background.png").convert()
    self.backgroundStage = pygame.image.load("resources/image/stage"+stage+"/background_game.png").convert()

  def changePaddle(self, stage):
    self.paddle.loadImage(stage)

  def changeBall(self, stage):
    self.balls[0].loadImage(stage)

  def changeBricks(self, stage):
    self.bricks = self.generateBricks(stage)


  def putBallToPaddle(self):
    self.balls[0].rect.midbottom = (self.paddle.rect.centerx, self.paddle.rect.y)

  def isOutOfScreen(self, obj):
    if(obj.rect.top >= SCREEN_SIZE[1]):
      return True
    if(obj.__class__ == Bullet and obj.rect.top < 0):
      return True
    return False

  def generateBricks(self, stage=1):
    bricks = []
    for x in range(43, 660, 60):
      for y in range(60, 300, 60):
        bricks.append(Brick(SCREEN_SIZE, x, y, stage))
    return bricks

  def isSwipe(self, gesture):
    gestureType = gesture.type
    if(gestureType is Leap.Gesture.TYPE_SWIPE ):
      # swipe = Leap.SwipeGesture(gesture)
      # print(swipe.direction)
      return True
    return False

  def eliminateObstacle(self, gesture):
    if(self.isSwipe(gesture) and len(self.specialItems) > 0):
      del self.specialItems[:]
    effectChannel = pygame.mixer.Channel(5)
    if(not effectChannel.get_busy()):
      effectChannel.play(self.sneeze)

  def shoot(self):
    if(self.paddle.bullet > 0 and len(self.bullet) < 1):
      self.paddle.bullet -= 1
      bullet = Bullet(self.paddle)
      self.bullet.append(bullet)

  def resetSpecialItemList(self):
    self.freeSlot = []
    for i in range(0, 1201):
      self.freeSlot.append(True)

  def appendBall(self):
    ball = Ball(self.backgroundStage, self.stage)
    ball.speed[1] = -ball.speed[1]
    ball.rect.midbottom = (self.paddle.rect.centerx, self.paddle.rect.y)
    self.balls.append(ball)

  def generateSpecialItem(self):
    now = self.time
    frequency = (20 - self.stage) + 1
    if(now % frequency == 0 and not self.specialItems and self.freeSlot[self.time]):
      self.specialItems.append(self.specialItemFactory.createItem(now, self.stage))
      self.freeSlot[self.time] = False

  def addBGMs(self):
    self.bgm = []
    for i in range(1, 5):
      music = pygame.mixer.Sound("resources/sound/BGMstage"+str(i)+".wav")
      self.bgm.append(music)


