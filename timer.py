import threading
import time

class Timer(threading.Thread):

    def __init__(self, gameState):
        threading.Thread.__init__(self)
        self.event = threading.Event()
        self.state = gameState

    def run(self):
        while self.state.time > 0 and not self.event.is_set():
            time.sleep(1)
            self.state.time -= 1

    def stop(self):
        self.event.set()
