
class Player():

  def __init__(self):
    self.life = 3
    self.score = 0
    self.highScore = 0

  def updateHighScore(self):
    self.highScore = max(self.score, self.highScore)