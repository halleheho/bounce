import pygame, random, time, math

class ItemFactory():

  def __init__(self):

    self.prepareList()
    self.lists = [self.itemListStage1,self.itemListStage2,
    self.itemListStage3,self.itemListStage4,self.itemListStage5]

  def prepareList(self):
    self.itemListStage1 = ["Expander","Shrinker"]
    self.itemListStage2 = list(self.itemListStage1)
    self.itemListStage2.extend(["Multiplier"])

    self.itemListStage3 = list(self.itemListStage2)
    self.itemListStage3.extend(["BallBooster","BallSlower"])

    self.itemListStage4 = list(self.itemListStage3)
    self.itemListStage4.extend(["BallExpander","BallShrinker"])
    self.itemListStage5 = list(self.itemListStage4)


  def createItem(self, x, y, stage):
    parsedStage = str(int(math.ceil(stage / 5.0)))
    self.types = self.getItems(stage)
    rand = random.randint(0, len(self.types)-1)
    constructor = globals()[self.types[rand]]
    return constructor(x, y, parsedStage)

  def getItems(self, stage):
    parsedStage = stage % 5
    return self.lists[parsedStage-1]


class SpecialItemFactory():


  def __init__(self):
    self.prepareList()
    self.lists = [self.itemListStage1,self.itemListStage2,
    self.itemListStage3,self.itemListStage4,self.itemListStage5]


  def createItem(self, spawnedTime, stage):
    parsedStage = str(int(math.ceil(stage / 5.0)))
    self.types = self.getItems(stage)
    rand = random.randint(0, len(self.types)-1)
    constructor = globals()[self.types[rand]]
    return constructor(spawnedTime, parsedStage)

  def prepareList(self):
    self.itemListStage1 = ["ScoreIncreaser","ScoreDecreaser"]
    self.itemListStage2 = list(self.itemListStage1)
    self.itemListStage2.extend(["LifeIncreaser","LifeDecreaser"])

    self.itemListStage3 = list(self.itemListStage2)
    self.itemListStage3.extend(["Gun"])

    self.itemListStage4 = list(self.itemListStage3)
    self.itemListStage4.extend(["Magic"])
    self.itemListStage5 = list(self.itemListStage4)


  def getItems(self, stage):
    parsedStage = int(math.ceil(stage/5.0))
    return self.lists[parsedStage-1]

class Item():

  name = "item"

  def __init__(self, x, y, stage):
    self.image = pygame.image.load("resources/image/item/stage"+stage+"/"+self.name+".png").convert_alpha()
    self.sound = pygame.mixer.Sound("resources/sound/"+self.name + ".wav")
    self.rect = self.image.get_rect()
    self.rect.centerx = x
    self.rect.centery = y
    self.duration = 10
    self.move()

  def onHit(self, gameState):
    return

  def move(self):
    return

class Expander(Item):
    name = "Expander"

    def onHit(self, gameState):
      gameState.paddle.expand()

    def move(self):
      self.rect.centery += 4

class Shrinker(Item):
  name = "Shrinker"

  def onHit(self, gameState):
    gameState.paddle.shrink()
  
  def move(self):
    self.rect.centery += 5

class Multiplier(Item):
  name = "Multiplier"

  def onHit(self, gameState):
    gameState.appendBall()

  def move(self):
    self.rect.centery += 10

class BallExpander(Item):
  name = "BallExpander"

  def onHit(self, gameState):
    balls = gameState.balls
    for ball in balls:
      ball.isBig = True
      ball.updateRect()

  def move(self):
    self.rect.centery += 7

class BallShrinker(Item):
  name = "BallShrinker"

  def onHit(self, gameState):
    balls = gameState.balls
    for ball in balls:
      ball.isBig = False
      ball.updateRect()
      
  def move(self):
    self.rect.centery += 6

class BallBooster(Item):
  name = "BallBooster"

  def onHit(self, gameState):
    balls = gameState.balls
    for ball in balls:
      ball.speedUp()

  def move(self):
    self.rect.centery += 5


class BallSlower(Item):
  name = "BallSlower"

  def onHit(self, gameState):
    balls = gameState.balls
    for ball in balls:
      ball.slowDown()

  def move(self):
    self.rect.centery += 5

class SpecialItem():

  def __init__(self, spawnedTime, stage):
    self.image = pygame.image.load("resources/image/item/"+self.name + ".png").convert_alpha()
    self.sound = pygame.mixer.Sound("resources/sound/"+self.name + ".wav")
    self.rect = self.image.get_rect()
    self.spawnedTime = spawnedTime
    self.stage = int(stage)
    rate = 4 - self.stage + 1
    self.duration = rate
    self.rect.x = 750
    self.rect.y = 302

  def effect(self, gameState):
    return

  def timeUp(self):
    if self.now <= self.spawnedTime - self.duration:
      return True
    return False    

  def afterTimeUp(self, gameState):
    return

  def update(self, now):
    self.now = now
    return

class LifeIncreaser(SpecialItem):
  name = "LifeIncreaser"

  def effect(self, gameState):
    gameState.player.life += 1
    self.sound.play()

class LifeDecreaser(SpecialItem):
  name = "LifeDecreaser"

  def effect(self, gameState):
      gameState.player.life -= 1
      self.sound.play()

  def afterTimeUp(self, gameState):
    self.effect(gameState)

class ScoreIncreaser(SpecialItem):
  name = "ScoreIncreaser"

  def effect(self, gameState):
    gameState.score += 500 * gameState.stage
    self.sound.play()

class ScoreDecreaser(SpecialItem):
  name = "ScoreDecreaser"

  def effect(self, gameState):
    gameState.score -= 500 * gameState.stage
    self.sound.play()

  def afterTimeUp(self, gameState):
    self.effect(gameState)

class Gun(SpecialItem):
  name = "Gun"

  def effect(self, gameState):
    gameState.paddle.bullet += 3
    self.sound.play()

class Magic(SpecialItem):
  name = "Magic"

  def effect(self, gameState):
    self.sound.play()
    for x in range(0, 3):
      if gameState.bricks:
        gameState.bricks.pop()










