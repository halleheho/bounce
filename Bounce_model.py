import pygame, random, time, math
from Bounce_item import *

class Paddle(pygame.sprite.Sprite):

  def __init__(self, screenWidth, stage=1):
    self.stage = str(int(math.ceil(stage/5.0)))
    self.size = 3
    self.bullet = 0
    self.loadImage("1")
    self.image = self.images[2]
    self.rect = self.image.get_rect()
    self.screenWidth = screenWidth.get_rect()
    self.rect.centerx = 500
    self.rect.centery = 500
    
  def loadImage(self, stage):
    self.images = []
    for i in range(1,6):
      img = pygame.image.load("resources/image/stage"+stage+"/paddle"+str(i)+".png").convert_alpha()
      self.images.append(img)
    self.image = self.images[self.size-1]

  def setPos(self, x, y):
    self.rect = self.image.get_rect()
    self.rect.x = x
    if(y < 300):
      self.rect.y = 300
    else:
      self.rect.y = y

  def expand(self):
    if(not self.size >= 5):
      self.size += 1
      self.image = self.images[self.size-1]

  def shrink(self):
    if(not self.size <= 1 ):
      self.size -= 1
      self.image = self.images[self.size-1] 

class Bullet(pygame.sprite.Sprite):
  
  def __init__(self, paddle):
    self.image = pygame.image.load("resources/image/stage2/bullet.png")
    self.rect = self.image.get_rect()
    self.paddle = paddle
    self.rect.centerx = self.paddle.rect.centerx
    self.rect.y = self.paddle.rect.y

  def move(self):
    self.rect.centery -= 10

class Ball(pygame.sprite.Sprite):

  def __init__(self, screenSize, stage=1):
    self.isBig = False
    self.speed = [7, -7]
    self.stage = str(int(math.ceil(stage/5.0)))
    self.loadImage(self.stage)
    self.image = pygame.image.load("resources/image/stage"+self.stage+"/ball.png").convert_alpha()
    self.rect = self.image.get_rect()
    x = random.randint(50, screenSize.get_width() - self.image.get_width())
    self.rect.midbottom = (x,500)
    self.screenSize = screenSize.get_rect()
    self.hitSound = pygame.mixer.Sound("resources/sound/crash.wav")
    self.randomTurn()
    
  def randomTurn(self):
    randList = [-1,1]
    x = random.randint(0,1)
    y = random.randint(0,1)
    self.speed[0] = self.speed[0] * randList[x]
    self.speed[1] = self.speed[1] * randList[y]

  def loadImage(self, stage=1):
    self.images = []
    smallList = []
    largeList = []


    for i in range(1,5):
      img = pygame.image.load("resources/image/stage"+str(stage)+"/ball"+str(i)+".png").convert_alpha()
      largeImg = pygame.image.load("resources/image/stage"+str(stage)+"/ballbig"+str(i)+".png").convert_alpha()
      smallList.append(img)
      largeList.append(largeImg)
    self.images.append(smallList)
    self.images.append(largeList)
    self.turn()

  def move(self):
    self.rect.centerx += self.speed[0]
    self.rect.centery += self.speed[1]
    self.turn()
    self.collideWall()

  def turn(self):
    index = self.parse(self.speed[0],self.speed[1])
    if(self.isBig):
      self.image = self.images[1][index-1]
    else:
      self.image = self.images[0][index-1]

  def speedUp(self):
    if(math.fabs(self.speed[0]) < 13.5):
      self.speed[0] = self.speed[0] * 2
      self.speed[1] = self.speed[1] * 2

  def slowDown(self):
    if(math.fabs(self.speed[0]) > 4.5):
      self.speed[0] = self.speed[0] / 2
      self.speed[1] = self.speed[1] / 2

  def updateRect(self):
    left = self.rect.left
    top = self.rect.top
    self.turn()
    width = self.image.get_width()
    height = self.image.get_height()
    self.rect = pygame.Rect((left, top), (width, height))

  def parse(self, speedX, speedY):
    if(speedX > 0 and speedY < 0):
      return 1
    elif(speedX < 0 and speedY < 0):
      return 2
    elif(speedX < 0 and speedY > 0):
      return 3
    else:
      return 4

  def jump(self):
    if(self.rect.centery > self.screenSize.height):
      self.speed[1] = math.fabs(self.speed[1]) * -1

  def collideWall(self):
    if (self.rect.topleft[0] <= self.screenSize.x+14):
      self.speed[0] = math.fabs(self.speed[0])
      self.turn()
      self.hitSound.play()
      
    elif (self.rect.topright[0] >=  self.screenSize.width-8):
      self.speed[0] = -math.fabs(self.speed[0])
      self.turn()
      self.hitSound.play()

    elif (self.rect.topleft[1] <= 14):
      self.speed[1] = math.fabs(self.speed[1])
      self.turn()
      self.hitSound.play()

class Cursor(pygame.sprite.Sprite):

  def __init__(self):
    self.pos = [0,0]
    self.image = pygame.image.load("resources/image/cursor.png").convert_alpha()
    self.image1 = pygame.image.load("resources/image/cursor.png").convert_alpha()
    self.image2 = pygame.image.load("resources/image/cursor2.png").convert_alpha()
    self.rect = self.image.get_rect()

  def setPos(self,x,y):
    self.rect.centerx = x
    self.rect.centery = y

class Brick(pygame.sprite.Sprite):

  def __init__(self, screenSize, x, y, stage=1):
    self.images = ['1','2','3','4','5','6','7','8']
    self.randomImage(stage)
    self.rect = self.image.get_rect()
    self.rect.centerx = x
    self.rect.centery = y
    self.screenSize = screenSize
    self.itemFactory = ItemFactory()

  def randomImage(self, stage):
    random.shuffle(self.images)
    self.image = pygame.image.load("resources/image/stage"+str(stage)+"/"+self.images[0]+".png").convert_alpha()

  def randomItem(self, x, y, stage): 
    rand = random.randint(0,100)
    randomRate = 5 - int(math.ceil(stage / 5.0))
    if (rand % randomRate == 0):
      return self.itemFactory.createItem(x, y, stage)
    return False

class Button(pygame.sprite.Sprite):

  name = "button"
  start = 441
  end = 776

  def __init__(self, x, y, bounce):
    self.image = pygame.image.load("resources/image/menu/"+self.name + ".png").convert_alpha()
    # self.sound = pygame.mixer.Sound("resources/sound/"+self.name + ".wav")

    self.rect = self.image.get_rect()
    self.rect.centerx = x
    self.rect.centery = y
    self.bounce = bounce

  def isAtTheEnd(self):
    if self.rect.centerx >= self.end:
      return True
    return False

  def setPos(self, x):
    if(x < self.start):
      self.rect.centerx = self.start
    elif(x > self.end):
      self.rect.centerx = self.end
    else:
      self.rect.centerx = x

  def goToNextState(self):
    return

class PlayButton(Button):
  name = "play"  

  def goToNextState(self):
    self.bounce.goToGameState()


class HowToButton(Button):
  name = "howto"

  def goToNextState(self):
    self.bounce.goToHowToState()

class CreditButton(Button):
  name = "credit"

  def goToNextState(self):
    self.bounce.goToCreditState()


class ExitButton(Button):
  name = "exit"

  def goToNextState(self):
    self.bounce.quit()

class BackButton(Button):
  name = "back"

  def goToNextState(self):
    self.bounce.goToMenuState()

class Rope(pygame.sprite.Sprite):

  def __init__(self, x, y):
    self.image = pygame.image.load("resources/image/howto/rope.png").convert_alpha()
    self.rect = self.image.get_rect()
    self.rect.centerx = x
    self.rect.centery = y
    self.start = self.rect.bottom
    self.end = self.start + 100


  def isAtTheEnd(self):
    if self.rect.bottom >= self.end:
      return True
    return False

  def setPos(self, y):
    if(y < self.start):
      self.rect.bottom = self.start
    elif(y > self.end):
      self.rect.bottom = self.end
    else:
      self.rect.bottom = y
